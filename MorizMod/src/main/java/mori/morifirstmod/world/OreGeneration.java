package mori.morifirstmod.world;

import mori.morifirstmod.lists.BlockList;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStage.Decoration;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.placement.CountRangeConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.registries.ForgeRegistries;

public class OreGeneration 
{
	public static void setupOreGeneration()
	{
		CountRangeConfig copper_ore_placement = new CountRangeConfig(85, 0, 0, 100);
		CountRangeConfig tin_ore_placement = new CountRangeConfig(85, 0, 0, 100);
		CountRangeConfig ruby_ore_placement = new CountRangeConfig(1, 0, 0, 20);
		CountRangeConfig amethyst_ore_placement = new CountRangeConfig(1, 0, 0, 20);
		CountRangeConfig saphire_ore_placement = new CountRangeConfig(1, 0, 0, 20);
		CountRangeConfig topaz_ore_placement = new CountRangeConfig(1, 0, 0, 20);
		CountRangeConfig lead_ore_placement = new CountRangeConfig(50, 0, 0, 63);
		CountRangeConfig aluminium_ore_placement = new CountRangeConfig(20, 0, 0, 40);
		CountRangeConfig silver_ore_placement = new CountRangeConfig(20, 0, 0, 50);
		CountRangeConfig darkmetal_ore_placement = new CountRangeConfig(2, 0, 0, 15);
		CountRangeConfig lightmetal_ore_placement = new CountRangeConfig(2, 0, 0, 15);
		CountRangeConfig rocksalt_ore_placement = new CountRangeConfig(30, 50, 0, 70);
		CountRangeConfig lumin_ore_placement = new CountRangeConfig(20, 20, 0, 45);
		
		
		
		for(Biome biome : ForgeRegistries.BIOMES)
		{	
			
			biome.addFeature(Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, 
                    BlockList.copper_ore.getDefaultState(), 13), Placement.COUNT_RANGE, copper_ore_placement));
			
			biome.addFeature(Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, 
                    BlockList.tin_ore.getDefaultState(), 13), Placement.COUNT_RANGE, tin_ore_placement));
			
			biome.addFeature(Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, 
                    BlockList.ruby_ore.getDefaultState(), 6), Placement.COUNT_RANGE, ruby_ore_placement));
			
			biome.addFeature(Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, 
                    BlockList.amethyst_ore.getDefaultState(), 6), Placement.COUNT_RANGE, amethyst_ore_placement));
			
			biome.addFeature(Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, 
                    BlockList.saphire_ore.getDefaultState(), 6), Placement.COUNT_RANGE, saphire_ore_placement));
			
			biome.addFeature(Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, 
                    BlockList.topaz_ore.getDefaultState(), 6), Placement.COUNT_RANGE, topaz_ore_placement));
			
			biome.addFeature(Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, 
                    BlockList.lead_ore.getDefaultState(), 10), Placement.COUNT_RANGE, lead_ore_placement));
			
			biome.addFeature(Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, 
                    BlockList.aluminium_ore.getDefaultState(), 10), Placement.COUNT_RANGE, aluminium_ore_placement));
			
			biome.addFeature(Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, 
                    BlockList.silver_ore.getDefaultState(), 10), Placement.COUNT_RANGE, silver_ore_placement));
			
			biome.addFeature(Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, 
                    BlockList.darkmetal_ore.getDefaultState(), 2), Placement.COUNT_RANGE, darkmetal_ore_placement));
			
			biome.addFeature(Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, 
                    BlockList.lightmetal_ore.getDefaultState(), 2), Placement.COUNT_RANGE, lightmetal_ore_placement));
			
			biome.addFeature(Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, 
                    BlockList.rocksalt_ore.getDefaultState(), 8), Placement.COUNT_RANGE, rocksalt_ore_placement));
			
			biome.addFeature(Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, 
                    BlockList.lumin_ore.getDefaultState(), 5), Placement.COUNT_RANGE, lumin_ore_placement));
		}
	}
		
}
