package mori.morifirstmod;

import mori.morifirstmod.lists.ItemList;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class FirstItemGroup extends ItemGroup
{
	public FirstItemGroup()
	{
		super("first");
	}

	@Override
	public ItemStack createIcon() 
	{
		
		return new ItemStack(ItemList.bronze_ingot);
	}
}
