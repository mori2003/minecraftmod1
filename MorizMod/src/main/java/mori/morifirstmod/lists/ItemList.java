package mori.morifirstmod.lists;

import net.minecraft.item.Item;

public class ItemList 
{
	public static Item bronze_ingot;
	public static Item tin_ingot;
	public static Item copper_ingot;
	public static Item ruby;
	public static Item amethyst;
	public static Item saphire;
	public static Item topaz;
	public static Item luminstone;
	public static Item rocksalt;
	
	public static Item beer;
	
	public static Item bronze_pickaxe;
	public static Item bronze_axe;
	public static Item bronze_pickaxeaxe;
	public static Item bronze_pickaxeaxeshovel;
	public static Item bronze_axeshovel;
	public static Item bronze_pickaxeshovel;
	public static Item bronze_hoe;
	public static Item bronze_shovel;
	public static Item bronze_sword;
	
	public static Item iron_pickaxeaxe;
	public static Item iron_pickaxeshovel;
	public static Item iron_pickaxeaxeshovel;
	public static Item iron_axeshovel;
	
	public static Item bronze_block;
	public static Item copper_ore;
	public static Item tin_ore;
	public static Item silver_ore;
	public static Item aluminium_ore;
	public static Item lead_ore;
	public static Item ruby_ore;
	public static Item saphire_ore;
	public static Item amethyst_ore;
	public static Item topaz_ore;
	public static Item darkmetal_ore;
	public static Item lightmetal_ore;
	public static Item rocksalt_ore;
	public static Item lumin_ore;

	public static Item cooking_pot;
}
